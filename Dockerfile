runtime - deps
postgresql - client
build - base
libxm12 - dev
libxslt - dev
nodejs
yarn
libffi - dev
readline
build - base
postgresql - dev \
libc - dev
linux - headers
readline - dev \
file
imagemagick
git
tzdato
&& rm -rf / var / cache / apk / *


WORKDIR / app
COPY . / app /
ENV BUNDLE_PATH / gems
RUN yarn install
RUN bundle install
ENTRYPOINT [ " bin / rails " ]
CMD [ " s " , " -b " , " 0.0.0.0 " ]
EXPOSE 3000