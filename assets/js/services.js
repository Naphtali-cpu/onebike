// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyA0wCZ4mUzloVxVQqO4TyN5EPGXiaO6-A0",
  authDomain: "bike-e4736.firebaseapp.com",
  projectId: "bike-e4736",
  storageBucket: "bike-e4736.appspot.com",
  messagingSenderId: "320059038546",
  appId: "1:320059038546:web:19c4a99f6f67a139d2788b"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Reference contactInfo collections
let contactInfo = firebase.database().ref("services");

// Listen for a submit and testing with console

document.querySelector(".information").addEventListener("submit", submitForm);

function submitForm(e) {
  e.preventDefault();

  let name = document.querySelector("#name").value;
  let location = document.querySelector("#location").value;
  let size = document.querySelector("#size").value;
  let phone = document.querySelector("#phone").value;

  console.log(name, location, size, phone);

  saveContactInfo(name, location, size, phone);
}

// Save Infos to Firebase
function saveContactInfo(name, location, size, phone) {
  let newContactInfo = contactInfo.push();

  newContactInfo.set({
    aName: name,
    bLocation: location,
    cPhone: phone,
    dType: size,

  })
}
